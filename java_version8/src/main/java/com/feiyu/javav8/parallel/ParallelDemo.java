package com.feiyu.javav8.parallel;

import java.util.stream.Stream;

/**
 * 并行流：使用的方法、实现原理、该规避的问题
 * 简化了多线程的操作
 */
public class ParallelDemo {
  public static void main(String[] args) {

    //parallel 将顺序流转换为，并行流,
    long commSum = commSum(1000);
    System.out.println("commSum:" + commSum);
    long parallelSum = parallelSum(1000);
    System.out.println("parallelSum:" + parallelSum);

    //顺序流 sequential 通过标记 parallel属性判断是否启动并行
    /**
     * 基本原理是使用： ForkJoinPool （分支/合并框架）分而治之
     *  1）默认的线程数为处理器核数， Runtime.getRuntime().availableProcessors()
     * */


    /**
     * 正确使用并行流
     *  1）错用并行流而产生错误的首要原因，就是使用的算法改变了某些共享状态
     *  2）forEach中调用的方法有副作用， 它会改变多个线程共享的对象的可变状态
     *  3）并行流避免使用共享可变状态
     * */
    long forEachSum = forEachSum(1000);
    System.out.println("forEachSum:"+forEachSum);


    /**
     * 高效的使用并行流：
     * 1）测试对比验证
     * 2）注意装箱核拆箱带来的性能损耗 原始类型流(IntStream、 LongStream、DoubleStream)来避免这种操作
     * 3）limit和findFirst等依赖于元 素顺序的操作，它们在并行流上执行的代价非常大，注意该类型操作
     * 4）数据量的小的时，并行造成的开销影响会更大
     * 5）并行流，终端合并的代价是否较大
     * */

  }


  public static long forEachSum(long n){
    Accumulator result = new Accumulator();
   Stream.iterate(1L, i -> i + 1).limit(n).parallel().forEach(result::add);
   return  result.total;
  };

  public static long parallelSum(long n) {
    return Stream.iterate(1L, i -> i + 1).limit(n).parallel().reduce(0l, Long::sum);
  }

  public static long commSum(long n) {
    long result = 0;
    for (long i = 1; i <=n; i++) {
      result += i;
    }
    return result;
  }
}
