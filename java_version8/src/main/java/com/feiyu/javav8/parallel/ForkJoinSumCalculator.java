package com.feiyu.javav8.parallel;

import java.util.concurrent.RecursiveTask;

/**
 * 1) 实现RecursiveTask 接口
 * 2）compute 逻辑进行任务拆分和执行子任务
 */
public class ForkJoinSumCalculator extends RecursiveTask<Long> {
  private final long[] numbers;//需要求和的数组
  private final int start;//子任务开始位置
  private final int end;//子任务截止位置

  public ForkJoinSumCalculator(long[] numbers) {
    this.numbers = numbers;
    this.start = 0;
    this.end = numbers.length;
  }

  public ForkJoinSumCalculator(long[] numbers, int start, int end) {
    this.numbers = numbers;
    this.start = start;
    this.end = end;
  }

  public static final long MIN_ARR_LEN = 10000;//子任务处理的数据大小

  @Override
  protected Long compute() {
    int len = end - start;
    //执行最小任务集
    if (len <= MIN_ARR_LEN) {
      return computeSequentially();
    }
    ForkJoinSumCalculator leftTask = new ForkJoinSumCalculator(numbers, start, start + len / 2);
    leftTask.fork();//ForkJoinPool 异步执行一个创建的子任务
    ForkJoinSumCalculator rightTask = new ForkJoinSumCalculator(numbers, start + len / 2, end);
    long rightResult = rightTask.compute();//同步执行第二个子任务
    Long leftResult = leftTask.join();//等待第一个子任务执行结果
    return rightResult + leftResult;

  }

  private long computeSequentially() {
    long sum = 0;
    for (int i = start; i < end; i++) {
      sum += numbers[i];
    }
    return sum;
  }
}
