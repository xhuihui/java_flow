package com.feiyu.javav8.parallel;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.stream.LongStream;

public class 分支合并框架 {
  public static void main(String[] args) throws ExecutionException, InterruptedException {
    long result = forkJoinSum(100000);
    System.out.println("forkJoinSum result :" + result);

    /**
     * 分支/合并框架处理任务的核心原理
     * 1）首先子任务会平均分配到 ForkJoinPool 子线程中
     * 2）当某个线程的任务队列都执行完成为空，而其它线程还在忙
     * 3）这时该空闲线程就会从随机选取一个线程的队列，从队尾获取一个任务执行
     * 在现实场景下，拆分的任务执行耗时并不是一样的；这样的机制有利于工作线程平衡负载同时避免资源浪费
     * */
    int defaultThreadNum = Runtime.getRuntime().availableProcessors();
    System.out.println("defaultThreadNum:" + defaultThreadNum);


    /**
      *  Spliterator（分流器） 定义如何拆分数据
      *  tryAdvance 方法判断是否还有遍历的元素
      *  trySplit 方法拆分元素
      *  estimateSize 还剩余多少需要遍历的元素
      * */


     /**
      * Java8 并行流就是基于 Spliterator将数据分流 交给 分支/合并框架并行处理
      * */
    Optional<Integer> option = Optional.of(1);
    option.isPresent();

    Future<String> future = Executors.newCachedThreadPool().submit(new Callable<String>() {

      @Override
      public String call() throws Exception {
        return "Hello ";
      }
    });

    future.get();
    future.isDone();
    future.get();

  }
  public static long forkJoinSum(long n){
     long[] numbers = LongStream.rangeClosed(1,n).toArray();
    ForkJoinSumCalculator task = new ForkJoinSumCalculator(numbers, 0, numbers.length);
   return new ForkJoinPool().invoke(task);
  }
}
