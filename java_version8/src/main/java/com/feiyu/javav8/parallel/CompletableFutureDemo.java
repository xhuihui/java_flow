package com.feiyu.javav8.parallel;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

public class CompletableFutureDemo {
  public static void main(String[] args) throws InterruptedException {

    //1、CompletableFuture 在执行异常和成功回调功能
    CompletableFuture<Integer> priceFuture = CompletableFuture.supplyAsync(CompletableFutureDemo::getPrice);
    //设置调用成功时的回调
    priceFuture.thenAccept((result) ->
        {
          System.out.println("the price " + result);
        }
    );

    //设置异常时的回调
    priceFuture.exceptionally((e) -> {
      System.out.println("get price ex :" + e.getMessage());
      return null;
    });

    Thread.sleep(1000);
    System.out.println("---------------");


    //2、多个具有依赖关系的任务串行执行
    CompletableFuture<String> queryFetch = CompletableFuture.supplyAsync(()-> CompletableFutureDemo.queryCode("中国建筑"));

    CompletableFuture<String> synPrice = queryFetch.thenApplyAsync((code) -> {
      return getPrice(code);
    });

    synPrice.thenAccept((result) ->{

      System.out.println("result :" +result );
    });


    System.out.println("---------------");
    //3、多个任务并行执行，只有有一个执行完成就返回结果
    // 两个CompletableFuture执行异步查询:
    CompletableFuture<String> cfQueryFromSina = CompletableFuture.supplyAsync(() -> {
      return queryCode("中国石油", "https://finance.sina.com.cn/code/");
    });
    CompletableFuture<String> cfQueryFrom163 = CompletableFuture.supplyAsync(() -> {
      return queryCode("中国石化", "https://money.163.com/code/");
    });

    // 用anyOf合并为一个新的CompletableFuture:
    CompletableFuture<Object> cfFetch = CompletableFuture.anyOf(cfQueryFromSina, cfQueryFrom163);
    
    // 最终结果:
    cfFetch.thenAccept((result) -> {
      System.out.println("final query code : " + result);
    });

    Thread.sleep(5000);




  }

  private static Double fetchPrice(String code, String url) {
    System.out.println("query price from " + url + "...");
    try {
      Thread.sleep((long) (Math.random() * 100));
    } catch (InterruptedException e) {
    }
    return 5 + Math.random() * 20;
  }

  public static  String queryCode(String name){
    return "NU_10001";
  }
  public static int getPrice() {
    int result = new Random().nextInt(100);
    if(result%2 != 0 ){
      throw  new RuntimeException("get Price Error");
    }
    return result;
  }

  public static String getPrice(String code) {
    int result = new Random().nextInt(100);
    return  code+" price "+result;
  }

  static String queryCode(String name, String url) {
    System.out.println("query code from " + name + "...");
    if("中国石油".equals(name)){
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    return name+"601857";
  }
}
