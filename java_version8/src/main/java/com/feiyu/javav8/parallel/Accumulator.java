package com.feiyu.javav8.parallel;

public class Accumulator {
  public long total = 0;
  public void add(long value) { total += value; }
}
