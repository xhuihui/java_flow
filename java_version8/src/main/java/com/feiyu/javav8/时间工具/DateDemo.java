package com.feiyu.javav8.时间工具;

import java.text.DateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class DateDemo {
  public static void main(String[] args) {
    //1、LocalDate 提供简单的日期信息
    LocalDate now = LocalDate.now();
    System.out.println("year:" + now.getYear());
    System.out.println("month:" + now.getMonth());
    System.out.println("week:" + now.getDayOfWeek());
    System.out.println("day:" + now.getDayOfMonth());
    //LocalDate 创建指定日期
    LocalDate ofDate = LocalDate.of(2021, 05, 21);

    //2、LocalTime 提供简单的时间信息
    LocalTime nowTime = LocalTime.now();
    System.out.println("hour:" + nowTime.getHour());
    System.out.println("minu:" + nowTime.getMinute());
    System.out.println("second:" + nowTime.getSecond());
    //LocalTime 创建指定的时间
    LocalTime ofTime = LocalTime.of(12, 30, 12);

    //3、LocalDateTime 集合了LocalDate、LocalTime  的所有功能
    LocalDateTime localDateTime = LocalDateTime.now();
    System.out.println("year:" + localDateTime.getYear());
    System.out.println("month:" + localDateTime.getMonth());
    System.out.println("week:" + localDateTime.getDayOfWeek());
    System.out.println("day:" + localDateTime.getDayOfMonth());
    System.out.println("hour:" + localDateTime.getHour());
    System.out.println("minu:" + localDateTime.getMinute());
    System.out.println("second:" + localDateTime.getSecond());


    //4、日期格式化, DateFormat 存在线程安全问题，LocalDateTime 优化了该细节
    LocalDateTime parseDateTime = LocalDateTime.parse("2021-05-12 12:30:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    System.out.println("formDateTime :" + parseDateTime.format( DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));


    //5、时间区间Duration 与 日期区间Period
    LocalDateTime  starTime = LocalDateTime.parse("2021-05-12 12:30:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    LocalDateTime  endTime = LocalDateTime.parse("2021-05-11 12:30:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

    long betweenHour = Duration.between(starTime, endTime).toHours();
    System.out.println("betweenHour:" + betweenHour);

    int  betweentDay = Period.between(starTime.toLocalDate(), endTime.toLocalDate()).getDays();
    System.out.println("betweentDay:"+betweentDay);


  }
}
