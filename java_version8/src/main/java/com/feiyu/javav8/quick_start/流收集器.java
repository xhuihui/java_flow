package com.feiyu.javav8.quick_start;

import com.feiyu.javav8.dto.Dish;

import java.util.Arrays;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 1、Java8 的流支持两类操作：1）中间操作 2）终端操作
 * 2、Stream collect 方法就是一个终端操作
 * * */
public class 流收集器 {
  public static void main(String[] args) {
    //收集器，Collector 常见的规约操作toList\toSet 等方法
    //归约：将流中所有元素反复结合起来，得到一个值（Int、list等），这类操作即为归约操作
    List<Dish> dishes = Dish.menu.stream().collect(Collectors.toList());

    //Collectors 收集器主要提供三大功能：1、将流元素归约和汇总为一个值 2、元素分组 3、元素分区

    //收集器：计数
    long countDish = Dish.menu.stream().collect(Collectors.counting());
    System.out.println("countDish:" + countDish);


    //收集器获取最大值、最小值
    Comparator<Dish> dishComparator = Comparator.comparingInt(Dish::getCalories);

    Optional<Dish>  maxDish = Dish.menu.stream().collect(Collectors.maxBy(dishComparator));
    System.out.println("maxDish:" + maxDish.get().getName());

    //收集器汇总:IntSummaryStatistics 、LongSummaryStatistics ： max、min、avg
    IntSummaryStatistics totalCalories = Dish.menu.stream().collect(Collectors.summarizingInt(Dish::getCalories));
    System.out.println("totalCalories:" +totalCalories.getCount());

    //收集器汇总，字符串链接
    String dishNames = Dish.menu.stream().map(Dish::getName).collect(Collectors.joining(","));
    System.out.println("dishNames:" + dishNames);
    
    //收集器，数据分组
    Map<Dish.Type, List<Dish>> dishType = Dish.menu.stream().collect(Collectors.groupingBy(Dish::getType));
    dishType.values().stream().forEach(dishs -> {
      dishs.stream().forEach(
           d  ->
           {
             System.out.println("type:" + d.getType() + " name:" + d.getName());
           }
       );
    });

    //分组统计
    Map<Dish.Type, Long> dishTypeCount = Dish.menu.stream().collect(Collectors.groupingBy(Dish::getType, Collectors.counting()));
    dishTypeCount.entrySet().stream().forEach(d ->
    {
      System.out.printf("type:" + d.getKey()  + " num:" + d.getValue() +"\t");
    });

    //分组统计，嵌套收集,ollectors.collectingAndThen 生成新的收集器
    //统计 每类菜中卡路里最高的，执行过程，先按照类型拆分为三个子流进行归约
    Map<Dish.Type, Dish> maxCalories = Dish.menu.stream().collect(
        Collectors.groupingBy(Dish::getType,
            Collectors.collectingAndThen(
                Collectors.maxBy(
                    Comparator.comparingInt(Dish::getCalories)
                ),
                Optional::get
            ))
    );
    maxCalories.entrySet().stream().forEach(v ->{
      System.out.printf("type:" + v.getKey()  + " maxCalories:" + v.getValue().getCalories() +"\t");
    });
    System.out.println("");

    //分区，partition 相对于分组，返回的Map key保留true、false两组元素
    //菜单，根据是否为素食分区
    Map<Boolean, List<Dish>> vegetarianPartition = Dish.menu.stream().collect(Collectors.partitioningBy(Dish::isVegetarian));

    vegetarianPartition.entrySet().stream().forEach(
        ds -> {
          System.out.println("vegetarian :" + ds.getKey() + ", values:" + ds.getValue());
        }
    );


    /**
     * 收集器顶级接口：Collector
     * 泛型：T 操作输入的数据类型   A 累加器收集过程中累积部分结果的对象 R 最终收集返回的对象
     * 几个接口的作用
     * **/

    //自定义收集器
    List<Integer> nums = Arrays.asList(1,2,3,4,5,6,7,8);
    Map<Boolean, List<Integer>> evenOdds = nums.stream().collect(new EvenOddCollector());
    evenOdds.entrySet().stream().forEach(e ->{
      System.out.println( (e.getKey()?"odd":"even") + ":" + e.getValue() );
    });
  }

}
