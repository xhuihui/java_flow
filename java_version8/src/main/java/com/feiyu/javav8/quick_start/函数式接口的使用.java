package com.feiyu.javav8.quick_start;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class 函数式接口的使用 {
  public static void main(String[] args) throws IOException {
    String filePath = "/opt/t_workspace/test_data/train.csv";
    //1、行为参数化
    String result = processFile(filePath, (BufferedReader br) -> br.readLine());
    System.out.println(result);
    result = processFile(filePath, (BufferedReader br) -> br.readLine() + br.readLine());
    System.out.println(result);


  }

  //2、定义函数式接口，并使用接口作为函数参数传递行为
  private static String processFile(String s, ReadFileProcess p) throws IOException {
    //3、执行行为
    BufferedReader bufferedReader = new BufferedReader(new FileReader(s));
    return p.process(bufferedReader);
  }
}

