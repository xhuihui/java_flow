package com.feiyu.javav8.quick_start;

import com.feiyu.javav8.dto.Apple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class 行为参数化传递 {
  public static void main(String[] args) {
    List<Apple> apps = Arrays.asList(new Apple(80,"red"),
        new Apple(155, "green"),
        new Apple(120, "red"));
    List<Apple>  filterApp ;

    //常规编码方式实现过滤
    filterApp = filterApple_1(apps,"red");
    System.out.println(filterApp);

    //行为参数化
    filterApp = filterApple_2(apps, new AppleRedolorPredicate());
    System.out.println(filterApp);

    //方法引用是行为参数化的特例，继承Predicate
    filterApp = filterApple_3(apps, 行为参数化传递::isRedApple);
    System.out.println(filterApp.toString());

    filterApp = filterApple_3(apps, apple -> apple.getColor().equals("red"));
    System.out.println(filterApp.toString());

    apps.sort((Apple a1, Apple a2) -> a1.getWeight().compareTo(a2.getWeight()));
    System.out.println(apps.toString());

    Thread t = new Thread(() -> System.out.println("hello world"));
    t.start();


  }


  public static boolean isRedApple(Apple apple){
    return "red".equals(apple.getColor());
  }

  //常规编码方式实现过滤
  public static List<Apple> filterApple_1(List<Apple> apples, String color){
     List<Apple>  filterApps = new ArrayList<>();
     for(Apple apple : apples){
         if(color.equals(apple.getColor())){
           filterApps.add(apple);
         }
     }
     return  filterApps;
  }

  //行为参数化
  public static List<Apple> filterApple_2(List<Apple> apples, ApplePredicate applePredicate){
    List<Apple>  filterApps = new ArrayList<>();
    for(Apple apple : apples){
      if(applePredicate.test(apple)){
        filterApps.add(apple);
      }
    }
    return  filterApps;
  }


  //方法应用，作为行为参数
  public static List<Apple> filterApple_3 (List<Apple> apples ,Predicate<Apple> predicate){
     List<Apple> filterApps = new ArrayList<Apple>();
     for(Apple apple : apples){
       if(predicate.test(apple)){
          filterApps.add(apple);
       }
     }
     return filterApps;
  }



}
