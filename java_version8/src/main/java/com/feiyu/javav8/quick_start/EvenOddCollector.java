package com.feiyu.javav8.quick_start;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 * 定义收集器
 * 奇偶数收集器
 * 1、约定 T, A, R 泛型
 * 2、实现归约过程
 */
public class EvenOddCollector implements Collector<Integer,
    Map<Boolean, List<Integer>>,
    Map<Boolean, List<Integer>>
    > {
  @Override
  public Supplier<Map<Boolean, List<Integer>>> supplier() {
    //初始化容器
   return ()-> new HashMap<Boolean, List<Integer>>(){
     {
       put(true, new ArrayList<>());
       put(false, new ArrayList<>());
     }
   };
  }

  //accumulator 定义了收集逻辑
  @Override
  public BiConsumer<Map<Boolean, List<Integer>>, Integer> accumulator() {
    BiConsumer<Map<Boolean, List<Integer>>, Integer> biConsumer = new BiConsumer<Map<Boolean, List<Integer>>, Integer>() {
      @Override
      public void accept(Map<Boolean, List<Integer>> booleanListMap, Integer integer) {
        boolean result =   integer % 2 == 0 ;
        booleanListMap.get(result).add(integer);
      }
    };
    return biConsumer;
  }

  //并行收集结果合并操作
  @Override
  public BinaryOperator<Map<Boolean, List<Integer>>> combiner() {
    BinaryOperator<Map<Boolean, List<Integer>>>   combiner =  new BinaryOperator<Map<Boolean, List<Integer>>>() {
      @Override
      public Map<Boolean, List<Integer>> apply(Map<Boolean, List<Integer>> booleanListMap, Map<Boolean, List<Integer>> booleanListMap2) {
        booleanListMap.get(true).addAll(booleanListMap2.get(true));
        booleanListMap.get(false).addAll(booleanListMap2.get(true));
        return booleanListMap;
      }
    };
    return combiner;
  }

  //返回的数据类型符合预期无需转换
  @Override
  public Function<Map<Boolean, List<Integer>>, Map<Boolean, List<Integer>>> finisher() {
    return Function.identity();
  }

  @Override
  public Set<Characteristics> characteristics() {
    return Collections.unmodifiableSet(EnumSet.of(Characteristics.IDENTITY_FINISH));
  }
}
