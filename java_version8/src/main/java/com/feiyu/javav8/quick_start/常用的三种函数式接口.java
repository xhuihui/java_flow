package com.feiyu.javav8.quick_start;

import javax.print.attribute.standard.PresentationDirection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class 常用的三种函数式接口 {
  public static void main(String[] args) {
    //1、predicate 函数式接口，test 方法返回boolean 类型数据，常用于判断过滤
    List<Integer> result = filter(Arrays.asList(1, 2, 3, 4, 5), (Integer num) -> num>2);
    System.out.println(result);

    //2、Consumer 函数式接口，accept 方法无返回值，常用于遍历执行操作的场景
    forEach(Arrays.asList(1, 2, 3, 4, 5), (Integer num) -> {
      System.out.println("--->" + num);
    });

    //3、Function 函数式接口，apply 方法返回转换后的结果，常用于数据类型转换
    List<Integer> strLen = map(Arrays.asList("adad", "lambda", "hello"), (String s) -> s.length());
    System.out.println(strLen);
  }



  public static <T> List<T> filter(List<T> list , Predicate<T> predicate){
    List<T>  result = new ArrayList<>();
    for(T s : list){
      if(predicate.test(s)){
        result.add(s);
      }
    }
    return  result;
  }

  public static <T>  void forEach(List<T> list , Consumer<T> consumer){
        for(T s : list){
          consumer.accept(s);
        }
  }

  public static <T,R>  List<R> map(List<T> list, Function<T,R> f){
     List<R>   result =  new ArrayList<>();
     for(T s: list){
         result.add(f.apply(s));
     }
     return  result;
  }
}
