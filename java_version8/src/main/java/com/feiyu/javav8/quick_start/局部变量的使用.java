package com.feiyu.javav8.quick_start;

public class 局部变量的使用 {

  public static String str = "23123";

  public static void main(String[] args) {
    final int num = 1330;
    Runnable run = () -> System.out.println(str);
    run.run();
    // lambda 表达式只能捕获局部变量一次,侧面的约束引用局部变量必须为final
    //num = 23123;
  }
}
