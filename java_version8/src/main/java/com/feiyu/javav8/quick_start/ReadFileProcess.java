package com.feiyu.javav8.quick_start;

import java.io.BufferedReader;
import java.io.IOException;

//定义函数式接口
@FunctionalInterface
public interface ReadFileProcess {
  String process(BufferedReader b) throws IOException;
}