package com.feiyu.javav8.quick_start;

import com.feiyu.javav8.dto.Apple;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class 复合Lambda表达式使用 {
    public static void main(String[] args) {
        List<Apple> apps = Arrays.asList(new Apple(80,"red"),
                new Apple(90,"yeelow"),
                new Apple(70,"green"));

        apps.sort(Comparator.comparing(Apple::getWeight));
        System.out.println(apps);
        //逆序
        apps.sort(Comparator.comparing(Apple::getWeight).reversed());
        System.out.println(apps);
        //比较链，按照重量递减&当重量一致时按照颜色排序
        apps.sort(Comparator.comparing(Apple::getWeight).reversed().thenComparing(Apple::getColor));
        System.out.println(apps);

       List<String> appleColor = apps.stream()
                                        .filter(apple -> {
                                            System.out.println("filter " + apple.hashCode());
                                            return apple.getWeight()>=80;})
                                        .map( apple -> {
                                            System.out.println("map " + apple.hashCode());
                                            return  apple.getColor();
                                        })
                                        .collect(Collectors.toList());
        System.out.println(appleColor);
    }
}
