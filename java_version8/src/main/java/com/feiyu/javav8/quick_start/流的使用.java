package com.feiyu.javav8.quick_start;

import com.feiyu.javav8.dto.Dish;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class 流的使用 {
    public static void main(String[] args) throws IOException {
        //筛选
        System.out.println("--------------筛选一张素食的菜单-----------------");
        List<Dish> vegetarianList = Dish.menu.stream()
                    .filter(Dish::isVegetarian)
                    .collect(Collectors.toList());
        System.out.println(vegetarianList);

        System.out.println("--------------筛选数组中奇数的数字,并除去重复结果数据 (数据去重)-----------------");
        List<Integer> numners= Arrays.asList(1,2,3,3,4,5,5,6,7);
        numners.stream().filter( num -> {
            if( num %2 !=0){
                System.out.println("filter num:" + num);
                return  true;
            }
            return false;
        }).distinct().forEach( num -> {
            System.out.println("for each " + num);
        });

        System.out.println("----------------筛选出热量超过300卡的前三道食物 （limit截取操作）-----------------");
         Dish.menu.stream().filter(d -> {
//            System.out.println("filter " + d.getName());
           return d.getCalories() > 300; })
//                          .limit(3)
                          .forEach( dish -> {
                              System.out.println("for each " + dish.getName());
                          });


        System.out.println("--------------卡路里超过300的菜跳过头2道（skip 跳过 ）------------------");
        Dish.menu.stream().filter(d -> d.getCalories() > 300)
                          .skip(2)
                          .forEach(d -> System.out.println("for each "+ d.getName()));


        //映射，指的是数据流的元素通过处理，会生成对应的一个新类似的数据
        System.out.println("--------------获取每道菜的名称------------------");
        List<String> dishNames = Dish.menu.stream().map(Dish::getName)
                          .collect(Collectors.toList());
        System.out.println(dishNames);

        System.out.println("--------------获取字符串集合中字符串的长度------------------");
       List<String> words = Arrays.asList("Java8","Java7","Hello Tom");
       List<Integer> wordLens = words.stream().map(String::length)
                                                .collect(Collectors.toList());
        System.out.println(wordLens);

        System.out.println("--------------将两个字符串的字符合并去重打印，映射方式处理（ 流的扁平化处理前 ）----------------------");
        //所谓扁平化数据处理就是将层级由多层降级到一层，便于处理
        words = Arrays.asList("GoodBye","Hello");
         words.stream().map(word -> word.split(""))
                 .map(Arrays::stream)
                .distinct()
                .forEach( s -> System.out.println(s.collect(Collectors.toList())));


        System.out.println("--------------将两个字符串的字符合并去重打印，流的扁平化处理（ 流的扁平化处理flatMap ）----------------------");
        words = Arrays.asList("GoodBye","Hello");
        words.stream().map(word -> word.split(""))
                .flatMap(Arrays::stream)  //流的扁平化
                .distinct()
                .forEach( s -> System.out.print(s));


        //元素查找与匹配，anyMatch、 findFirst和findAny这三个操作都用到了我们所谓的短路，只要一个元素符合预期后面的元素就不用处理了
        System.out.println("--------------元素查找与匹配，至少匹配一个元素 anyMatch----------------------");
        //确认菜单里至少包含一个素食
        if(Dish.menu.stream().anyMatch(Dish::isVegetarian)){
            System.out.println("菜单中包含有素食");
        }

        System.out.println("--------------元素查找与匹配，所有元素都匹配 allMatch----------------------");
        if(Dish.menu.stream().allMatch(d -> d.getCalories()<1000)){
            System.out.println("菜单中所有菜品均小于1000卡路里");
        }

        System.out.println("--------------元素查找与匹配，所有元素都不匹配 noneMatch----------------------");
        if(Dish.menu.stream().noneMatch(d -> d.getCalories()>=1000 )){
            System.out.println("菜单中没有大于1000卡路里的菜品");
        }

        System.out.println("--------------元素查 optional 容器表示值的存在与不存避免NUll问题----------------------");
        Optional<Dish> optional = Dish.menu.stream().filter(Dish::isVegetarian).findAny();
        Dish dish = optional.get();//获取对应的值
        System.out.println(dish);
        //元素存在则执行，代码块中的代码
        optional.ifPresent(d -> System.out.println(d.getName()));
        Dish defaultDish =  optional.orElse(Dish.menu.get(0));//没有符合预期的值则返回指定默认值
        System.out.println(defaultDish.toString());
        if(optional.isPresent()){
            System.out.println("菜单中包含有素食的菜品");
        }

        System.out.println("--------------元素查找与匹配，查找流/集合中第一个元素----------------------");
        //找出集合中平方后能被3整除的第一个元素
        List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5);
        Optional<Integer> first = integerList.stream().map(num -> num * num)
                .filter(num -> num % 3 == 0)
                .findFirst();
        System.out.println("集合中第一个符合要求的元素："+Math.sqrt(first.get()));


      System.out.println("--------------数值流， IntStream、DoubleStream和 LongStream---------------------");
      /***
       * Java8 针对int、long、double 有对应 IntStream、DoubleStream和 LongStream 三个流对象
       * 但是有时需要直接将流中的元素特化为int、long和double，Java8 也提供了对应的函数进行转换
       */

      //默认
      Stream<Integer> integerStream = Dish.menu.stream().map(d -> d.getCalories());
      integerStream.mapToInt(d -> d).sum();

      //函数转换
      int total = Dish.menu.stream().mapToInt(d-> d.getCalories()).sum();
      System.out.println(total);

      //转换为对象流
      IntStream intStream = Dish.menu.stream().mapToInt(d -> d.getCalories());
      intStream.sum();


      System.out.println("--------------数值流，数值范围--------------------");
      IntStream rangeInt = IntStream.range(1, 100);//不包含结束值
      System.out.println(rangeInt.count());

       rangeInt = IntStream.rangeClosed(1, 100);//包含结束值
      System.out.println(rangeInt.count());


      System.out.println("--------------常见的流，构建--------------------");
      //Stream.of 根据现有值创建流
      Stream<String> startem = Stream.of("java 8", "Lambdas", "Action");
      startem.map(String::toLowerCase).forEach(System.out::printf);
      //空流
      Stream<Object> emptyStream = Stream.empty();
      System.out.println();

      //由字数组创建的流
      int[] numbers = {1,2,3,4,5};
      int max = Arrays.stream(numbers).max().getAsInt();
      System.out.println("array stream  max num :" + max );

       //文件流,Files.lines每次读取一行数据出来
      Stream<String> fileStream = Files.lines(Paths.get("/opt/t_workspace/test_data/train.csv"));
      fileStream.forEach(System.out::println);


      //无限流，指的是无固定大小的流
      //创建方式有两种： Stream.iterate 接收表达式创建无限流，Stream.generate 接收方法创建无限流
      //偶数流
      Stream.iterate(0,n-> n+2).limit(20).forEach(num ->{
          System.out.print(num + ",");
      });

      System.out.printf("");
      //接收一个方法创建无限流
      Stream.generate(Math::random).limit(100).forEach(num ->{
        System.out.print(num + ",");
      });
    }
}
