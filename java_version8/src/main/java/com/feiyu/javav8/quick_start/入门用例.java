package com.feiyu.javav8.quick_start;

import java.io.File;
import java.io.FileFilter;

public class 入门用例 {

  public static void main(String[] args) {

    getHiddenFile();


  }


  //1、方法作为引用案例
  public static void getHiddenFile() {

    //传统的编码方式
//    File[] files = new File("/opt/app").listFiles(new FileFilter() {
//      @Override
//      public boolean accept(File pathname) {
//        return pathname.isHidden();
//      }
//    });

    //使用JDK8 方法引用 传递
    File[] files = new File("/opt/app").listFiles(File::isHidden);

    for (File file : files) {
      System.out.println(file.toString());
    }
  }



}
