package com.feiyu.javav8.quick_start;

import com.feiyu.javav8.dto.Apple;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class 构造函数引用 {
  public static void main(String[] args) {
    //方法引用创建构造函数,Supplier 函数接口
    Supplier<Apple> c1 = Apple::new;
    //调用get 方法创建一个
    Apple apple = c1.get();

    //lambda 表达式创建默认构造函数
    c1 = () -> new Apple();
    apple = c1.get();


    //Funcation 创建构造函数
    Function<Integer, Apple> c2 = Apple::new;
    c2.apply(100);

    //BiFuncton 创建两个参数构造函数
    BiFunction<Integer,String, Apple> c3 = Apple::new;
    c3.apply(110,"red");

  }
}
