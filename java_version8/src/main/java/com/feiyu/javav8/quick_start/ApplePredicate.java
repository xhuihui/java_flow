package com.feiyu.javav8.quick_start;

import com.feiyu.javav8.dto.Apple;

public interface ApplePredicate {
  boolean test (Apple apple);
}
