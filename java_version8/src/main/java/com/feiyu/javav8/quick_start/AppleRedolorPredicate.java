package com.feiyu.javav8.quick_start;

import com.feiyu.javav8.dto.Apple;

public class AppleRedolorPredicate implements  ApplePredicate {
  @Override
  public boolean test(Apple apple) {
    return "red".equals(apple.getColor());
  }
}
