package com.feiyu.sws;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.feiyu.sws.dao.User;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

public class ReqSws {
  public static void main(String[] args) {
    JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
    Client client = dcf.createClient("http://localhost:8080/webservice/user?wsdl");
    Object[] objects = new Object[0];
    ObjectMapper mapper = new ObjectMapper();
    try {
      // invoke("方法名",参数1,参数2,参数3....);
//      User user  = new User();
////      String userXml = "user-xml";
      objects = client.invoke("getUser", "tom");
      System.out.println(mapper.writeValueAsString(objects[0]));
    } catch (java.lang.Exception e) {
      e.printStackTrace();
    }
  }
}
