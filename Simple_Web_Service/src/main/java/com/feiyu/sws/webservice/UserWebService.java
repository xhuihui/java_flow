package com.feiyu.sws.webservice;


import com.feiyu.sws.dao.User;

import javax.jws.WebService;

@WebService(name = "UserWebService", // 暴露服务名称
    targetNamespace = "http://service.webservice.feiyu.com"// 命名空间,一般是接口的包名倒序
)
public interface UserWebService {
   User getUser(String userName);
}

