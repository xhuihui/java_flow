package com.feiyu.sws.webservice.impl;

import com.feiyu.sws.dao.User;
import com.feiyu.sws.webservice.UserWebService;
import org.springframework.stereotype.Service;

import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.SOAPBinding;


@Service
@WebService(serviceName = "UserWebService", // 与接口中指定的name一致
    targetNamespace = "http://service.webservice.feiyu.com", // 与接口中的命名空间一致
    endpointInterface = "com.feiyu.sws.webservice.UserWebService" // 接口地址
)
@BindingType(SOAPBinding.SOAP12HTTP_BINDING)
public class UserWebServiceImpl implements UserWebService {
  @Override
  public User getUser(String userName) {
    System.out.printf("userName" + userName);
    return new User("杰克马",12);
  }
}
