package com.feiyu.sws.dao;

import lombok.Data;
import lombok.Getter;

@Data
public class User {
  private String name;
  private int age;

  public User(String name, int age) {
    this.name = name;
    this.age = age;
  }
}
