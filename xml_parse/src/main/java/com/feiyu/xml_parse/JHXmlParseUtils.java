package com.feiyu.xml_parse;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 * 嘉和 WebService 接口Xml 数据解析
 */
public class JHXmlParseUtils {

  private static final Logger LOGGER = Logger.getLogger("JHXmlParseUtils");

  public static void main(String[] args) throws IOException, DocumentException {
    BufferedReader read = new BufferedReader(new FileReader("/opt/t_workspace/test_data/JH0701F01.xml"));
    StringBuffer lines = new StringBuffer();
    String line;
    while ((line = read.readLine()) != null) {
      lines.append(line).append("\n");
    }
    read.close();
    parseResult(lines.toString());
  }

  /**
   * @param respResult 解析请求结果
   */
  public static boolean parseResult(String respResult) throws DocumentException {
    SAXReader reader = new SAXReader();
    Document document = reader.read(new ByteArrayInputStream(respResult.getBytes()));
    Element resultElement = document.getRootElement().element("acknowledgement");
    String result = resultElement.attribute("typeCode").getValue();
    if ("AA".equals(result)) {
      return true;
    }
    Element acknowledgementDetail = resultElement.element("acknowledgementDetail");
    String failReson = acknowledgementDetail.element("text").getData().toString();
    LOGGER.info("请求接口失败，失败原因:" + failReson);

    String xml ="\n" +
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:good=\"http://goodwillcis.com\">\n" +
        "   <soapenv:Header/>\n" +
        "   <soapenv:Body>\n" +
        "      <good:HIPMessageServer>\n" +
        "         <good:Message>123</good:Message>\n" +
        "      </good:HIPMessageServer>\n" +
        "   </soapenv:Body>\n" +
        "</soapenv:Envelope>";
    return false;
  }
}
